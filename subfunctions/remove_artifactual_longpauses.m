function [ibi_corrected,response_predictions]=remove_artifactual_longpauses(ibi,ibi_time,...
    sats_time,hr_time,sats,hr,filtered_ip_time,filtered_ip)
%
%Applies machine learning output to IBI longer than 15 seconds, and removes
%those that are artifactually low ampltidue due to shallow breathing or
%poor electrode placement
%
% Input:    ibi - sequence of interbreath intervals
%           ibi_time corresponding times in seconds
%           sats_time - time vector for sats rate values 
%           hr_time - time vector for heart rate values 
%           sats - values for oxygen saturation
%           hr - values for heart rate
%           filtered_ip_time - time vector for filtered IP signal
%           filtered_ip - IP signal filtered according to ....
%
% Output:   ibi_corrected - ibi sequence with long pauses that aren't real
%               set to NaN
%           response_predictions - value of 1 corresponds to IBI>=20 seconds not
%               classed as real by the model, 2 is those that are real
%
% Caroline Hartley January 2021
%
% If you use this code please cite it as
% Adjei T, Purdy R, Jorge J, et al
% New method to measure interbreath intervals in infants for the assessment of apnoea and respiration
% BMJ Open Respiratory Research 2021;8:e001042. doi: 10.1136/bmjresp-2021-001042
% 
% For further help please email Associate Prof. Caroline Hartley (caroline.hartley@paediatrics.ox.ac.uk).


%load model - change file path accordingly

current_folder=pwd;
addpath([current_folder]);

load('model_for_apnoeas.mat');


%find ibi >=20 seconds
x=20;
ind_longpause=find(ibi>=x);
if ~isempty(ind_longpause)
    if ind_longpause(1)==1
        ind_longpause(1)=[];
    end

    long_pausest=ibi_time(ind_longpause-1); %start and end times of pause
    long_pauseen=ibi_time(ind_longpause);

    %initialise vectors
    mean_dur=zeros(length(ind_longpause),1); mean_pre=zeros(length(ind_longpause),1); mean_post=zeros(length(ind_longpause),1);
    all_sats=zeros(length(ind_longpause),1); all_hr=zeros(length(ind_longpause),1);


    for i=1:length(long_pausest)
        %find change in sats and heart rate around pause
        ind_satspost=find(sats_time >=long_pausest(i) & sats_time <=long_pausest(i)+60);
        ind_satspre=find(sats_time>=long_pausest(i)-10 & sats_time <=long_pausest(i));
        ind_hrpost=find(hr_time >=long_pausest(i) & hr_time <=long_pausest(i)+60);
        ind_hrpre=find(hr_time>=long_pausest(i)-10 & hr_time <=long_pausest(i));

            if isempty(ind_satspost) | isempty(ind_satspre) | isempty(ind_hrpre) | isempty(ind_hrpost)
                continue
            end

        change_sats=(mean(sats(ind_satspre))-min(sats(ind_satspost)));
        change_hr=(mean(hr(ind_hrpre))-min(hr(ind_hrpost)));

        % calculate rms of IP signal during pause
        tp=intersect(find(filtered_ip_time>(long_pausest(i)+1)),find(filtered_ip_time<(long_pausest(i)+(x-1))));
         %split into x-2 equal sections - changed from original code due to
         %shorter apnoea length
        f=floor(length(tp)/(x-2));
        m_cut=zeros((x-2),1);

        for cut=1:(x-2)
            tpcut=tp(f*(cut-1)+1:f*cut);
            scut=filtered_ip(tpcut);
            m_cut(cut)=rms(scut(~isnan(scut)));
        end

        %values of IP signal before and after pause
        tp1=intersect(find(filtered_ip_time<(long_pausest(i))),find(filtered_ip_time>(long_pausest(i)-10)));
        s1=filtered_ip(tp1);

        tp2=intersect(find(filtered_ip_time>(long_pauseen(i))),find(filtered_ip_time<(long_pauseen(i)+10)));
        s2=filtered_ip(tp2);

        %store values in vectors
        mean_dur(i)=median(m_cut,'omitnan');
        mean_pre(i)=rms(s1(~isnan(s1)));
        mean_post(i)=rms(s2(~isnan(s2)));

        all_sats(i)=change_sats;
        all_hr(i)=change_hr;

    end

    %apply model

    predictors=cat(2,mean_dur,mean_pre,mean_post,all_sats,all_hr);

    response_predictions = predict(mdl, predictors);

    %predictions - 2 is real pause, 1 if not real.
    %remove those that are not real from ibi by setting to NaN

    ibi_remove_index=find(response_predictions==1);
    ibi_corrected=ibi;
    ibi_corrected(ind_longpause(ibi_remove_index))=NaN;

else
    ibi_corrected=ibi;
    response_predictions=[];
end
