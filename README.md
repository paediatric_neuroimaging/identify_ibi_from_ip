This codes identifies the inter-breath intervals (IBIs) from an impedance pneumograph (IP) signal, as described in Adjei et al. 2021. 

To use this code the entire repository should be downloaded, retaining the same file structure seen in the repository. The files titled 'remove_artifactual_longpauses.m' and 'model_for_apnoeas.mat' should be kept within the 'subfunctions' subfolder.

The main function to use is ip_to_interbreath_intervals. Please read the guidance notes at the starts of 'ip_to_interbreath_intervals.m' before using this function. This describes the expected inputs and outputs.

The 'remove_artifactual_longpauses.m' subfunction corrects the inter-breath interval series through the removal of artefactually long IBIs; the subfuntion uses the classification model 'model_for_apnoeas.mat' to do this. This function is called from the main function.

If you use this code please cite 
Adjei T, Purdy R, Jorge J, et al
New method to measure interbreath intervals in infants for the assessment of apnoea and respiration
BMJ Open Respiratory Research 2021;8:e001042. doi: 10.1136/bmjresp-2021-001042

For further help please email Associate Prof. Caroline Hartley (caroline.hartley@paediatrics.ox.ac.uk).
